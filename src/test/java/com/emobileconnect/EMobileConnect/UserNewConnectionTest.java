package com.emobileconnect.EMobileConnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.emobileconnect.controller.UserNewConnectionController;
import com.emobileconnect.dto.UserNewConnectionDTO;
import com.emobileconnect.service.UserNewConnectionService;

@ExtendWith(MockitoExtension.class)
public class UserNewConnectionTest {

	@Mock
	UserNewConnectionService userNewConnectionService;

	@InjectMocks
	UserNewConnectionController userController;

	static UserNewConnectionDTO userNewConnectionDTO;

	@BeforeAll
	public static void setUp() {
		userNewConnectionDTO = new UserNewConnectionDTO();
		userNewConnectionDTO.setAge(20);
		userNewConnectionDTO.setEmailId("sat@gmail.com");
		userNewConnectionDTO.setGender("male");
		userNewConnectionDTO.setUserName("satish");
	}

	@Test
	@DisplayName("FoodItem function: Positive Scenerio")
	public void userConnectionTest() {
		UserNewConnectionDTO userNewConnectionDTO = new UserNewConnectionDTO();
		when(userNewConnectionService.saveNewUserConnection(userNewConnectionDTO)).thenReturn(userNewConnectionDTO);
		ResponseEntity<UserNewConnectionDTO> result = userController.newConnection(userNewConnectionDTO);
		verify(userNewConnectionService).saveNewUserConnection(userNewConnectionDTO);
		assertEquals(userNewConnectionDTO, result.getBody());
	}
	
	@Test
	@DisplayName("FoodItem function: Negative Scenerio")
	public void userNegativeConnectionTest() {
		UserNewConnectionDTO userNewConnectionDTO = new UserNewConnectionDTO();
		when(userNewConnectionService.saveNewUserConnection(userNewConnectionDTO)).thenReturn(userNewConnectionDTO);
		ResponseEntity<UserNewConnectionDTO> result = userController.newConnection(userNewConnectionDTO);
		verify(userNewConnectionService).saveNewUserConnection(userNewConnectionDTO);
		assertEquals(userNewConnectionDTO, result.getBody());
	}
}
