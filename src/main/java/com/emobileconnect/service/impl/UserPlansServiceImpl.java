package com.emobileconnect.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;
import com.emobileconnect.repository.UserPlansRepository;
import com.emobileconnect.service.UserPlansService;


@Service
public class UserPlansServiceImpl implements UserPlansService {
	private static final Logger logger = LoggerFactory.getLogger(UserPlansServiceImpl.class);
	
	@Autowired
	UserPlansRepository userPlansRepository;

	@Override
	public List<UserPlans> getUserPlansbyRequestId(@Valid Integer requestId, int pageNumber, int pageSize) throws UserPlansNotFoundException {
		
		Page<UserPlans> plans;
		List<UserPlans> userPlans=new ArrayList<UserPlans>();
		Pageable pageable=PageRequest.of(pageNumber, pageSize);
		plans=userPlansRepository.findUserPlansByRequestId(requestId,pageable);
		if(plans.isEmpty()) {
			logger.warn("The User is not requested any plan,you need to request");
			throw new UserPlansNotFoundException("The User RequestId is not Requested any plan");
		}
		
		plans.stream().forEach(plan ->userPlans.add(plan));
		
		return userPlans;
	}

}
