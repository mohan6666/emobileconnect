package com.emobileconnect.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emobileconnect.dto.UserNewConnectionDTO;
import com.emobileconnect.entity.UserDetails;
import com.emobileconnect.repository.UserNewConnectionRepository;
import com.emobileconnect.service.UserNewConnectionService;

@Service
public class UserNewConnectionServiceImpl implements UserNewConnectionService {
	private static final Logger logger = LoggerFactory.getLogger(UserNewConnectionServiceImpl.class);

	@Autowired
	private UserNewConnectionRepository userNewConnectionRepo;

	@Override
	public UserNewConnectionDTO saveNewUserConnection(UserNewConnectionDTO userNewConnectionDto) {
		logger.debug("Started usernewconnection class");
		UserDetails userConnection = new UserDetails();
		
		BeanUtils.copyProperties(userNewConnectionDto, userConnection);
		userNewConnectionRepo.save(userConnection);
		logger.debug("Ended usernewconnection class");
		
		return userNewConnectionDto;
	}

}
