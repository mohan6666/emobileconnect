package com.emobileconnect.service;

import com.emobileconnect.dto.UserNewConnectionDTO;

public interface UserNewConnectionService {


	public UserNewConnectionDTO saveNewUserConnection(UserNewConnectionDTO userNewConnectionDto);

}
