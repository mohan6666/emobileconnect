package com.emobileconnect.service;

import java.util.List;

import javax.validation.Valid;

import com.emobileconnect.entity.UserPlans;
import com.emobileconnect.exceptions.UserPlansNotFoundException;

public interface UserPlansService {

	List<UserPlans> getUserPlansbyRequestId(@Valid Integer requestId, int pageNumber, int pageSize) throws UserPlansNotFoundException;

}
