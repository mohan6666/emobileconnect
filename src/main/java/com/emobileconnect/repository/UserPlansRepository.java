package com.emobileconnect.repository;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobileconnect.entity.UserPlans;

@Repository
public interface UserPlansRepository extends JpaRepository<UserPlans, Integer> {

	Page<UserPlans> findUserPlansByRequestId(@Valid Integer requestId, Pageable pageable);

	
}
