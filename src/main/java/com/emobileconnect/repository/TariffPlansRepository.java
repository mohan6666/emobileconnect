package com.emobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobileconnect.entity.TariffPlans;

@Repository
public interface TariffPlansRepository extends JpaRepository<TariffPlans, Integer> {

}
