package com.emobileconnect.exceptions;

public class TariffPlansNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public TariffPlansNotFoundException(String message) {
			super(message);
		}

	public TariffPlansNotFoundException(String message, Throwable t) {
			super(message, t);
		}

}
